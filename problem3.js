function problem3(arrayOfObjects) {
  try {
    if (!Array.isArray(arrayOfObjects)) {
      throw new Error("Input is not an array");
    }

    let students_live_in_aus = [];

    for (let index = 0; index < arrayOfObjects.length; index++) {
      const item = arrayOfObjects[index];

      if (typeof item !== "object" || item === null) {
        throw new Error(`Invalid object at index ${index}`);
      }

      if (typeof item.name !== "string" || !item.name.trim()) {
        throw new Error(`Name is not valid for object at index ${index}`);
      }

      if (typeof item.isStudent !== "boolean") {
        throw new Error(`isStudent is not valid for object at index ${index}`);
      }

      if (typeof item.country !== "string") {
        throw new Error(`Country is not valid for object at index ${index}`);
      }

      if (item.isStudent && item.country === "Australia") {
        students_live_in_aus.push(item.name);
      }
    }

    for (let index = 0; index < students_live_in_aus.length; index++) {
      console.log(students_live_in_aus[index]);
    }

  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem3;
