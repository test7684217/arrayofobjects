function problem2(arrayOfObjects) {
  try {
    if (!Array.isArray(arrayOfObjects)) {
      throw new Error("Input is not an array");
    }

    let hobbiesofAge30 = [];

    for (let arrayIndex = 0; arrayIndex < arrayOfObjects.length; arrayIndex++) {
      const currentItem = arrayOfObjects[arrayIndex];

      if (typeof currentItem !== "object" || currentItem === null) {
        throw new Error("Invalid object in array");
      }

      if (typeof currentItem.age !== "number") {
        throw new Error("Age is not a number");
      }

      if (!Array.isArray(currentItem.hobbies)) {
        throw new Error("Hobbies is not an array");
      }

      if (currentItem.age === 30) {
        for (let hobbyIndex = 0; hobbyIndex < currentItem.hobbies.length; hobbyIndex++) {
          hobbiesofAge30.push(currentItem.hobbies[hobbyIndex]);
        }
      }
    }

    for (let hobbyIndex = 0; hobbyIndex < hobbiesofAge30.length; hobbyIndex++) {
      console.log(hobbiesofAge30[hobbyIndex]);
    }

  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem2;
