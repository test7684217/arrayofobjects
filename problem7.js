function problem7(arrayOfObjects) {
  try {
    if (!Array.isArray(arrayOfObjects)) {
      throw new Error("Input is not an array");
    }

    for (let index = 0; index < arrayOfObjects.length; index++) {
      const item = arrayOfObjects[index];
      if (item.age && item.age === 25 && item.name && item.email) {
        console.log(`Name: ${item.name}`, `Email: ${item.email}`);
      }
    }

  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem7;
