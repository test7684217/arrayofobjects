function problem6(arrayOfObjects) {
  try {
    if (!Array.isArray(arrayOfObjects)) {
      throw new Error("Input is not an array");
    }

    let firstHobbies = [];
    for (let index = 0; index < arrayOfObjects.length; index++) {
      const item = arrayOfObjects[index];
      if (item.hobbies && item.hobbies.length > 0) {
        firstHobbies.push(item.hobbies[0]);
      } else {
        throw new Error("No hobbies defined for an object");
      }
    }

    for (let index = 0; index < firstHobbies.length; index++) {
      console.log(firstHobbies[index]);
    }
  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem6;
