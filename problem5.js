function problem5(arrayOfObjects) {
  try {
    if (!Array.isArray(arrayOfObjects)) {
      throw new Error("Input is not an array");
    }

    const ages = [];
    for (let index = 0; index < arrayOfObjects.length; index++) {
      const obj = arrayOfObjects[index];
      if (obj.age !== undefined) {
        ages.push(obj.age);
      }
    }

    for (let index = 0; index < ages.length; index++) {
      console.log(ages[index]);
    }

  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem5;
