function problem4(arrayOfObjects) {
  try {
    if (!Array.isArray(arrayOfObjects)) {
      throw new Error("Input is not an array");
    }

    if (arrayOfObjects.length < 4) {
      throw new Error("Input array size should be greater than 3");
    }

    let individual = arrayOfObjects[3];
    console.log(`Name: ${individual.name},`, `City: ${individual.city}`);

  } catch (error) {
    console.log(error.message);
  }
}


module.exports = problem4