function problem8(arrayOfObjects) {
    try {
      if (!Array.isArray(arrayOfObjects)) {
        throw new Error("Input is not an array");
      }
  
      for (let index = 0; index < arrayOfObjects.length; index++) {
        if (arrayOfObjects[index].city && arrayOfObjects[index].country) {
          console.log(
            `City: ${arrayOfObjects[index].city},`,
            `Country: ${arrayOfObjects[index].country}`
          );
        }
      }
    } catch (error) {
      console.error(error.message);
    }
  }
  
  module.exports = problem8;
  