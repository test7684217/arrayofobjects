function problem1(arrayOfObjects) {
    try {
        if (!Array.isArray(arrayOfObjects)) {
            console.error("Input is not an array");
            return [];
        }

        if (arrayOfObjects.length === 0) {
            console.error("Input array is empty");
            return [];
        }

        let emails = [];

        for (let index = 0; index < arrayOfObjects.length; index++) {
            const item = arrayOfObjects[index];

            if (typeof item !== 'object' || item === null) {
                console.error(`Invalid object at index ${index}`);
                return [];
            }

            if (!item.hasOwnProperty('email')) {
                console.error(`Email is not defined for object at index ${index}`);
                return [];
            }

            if (typeof item.email !== 'string' || !item.email.trim()) {
                console.error(`Email is not valid for object at index ${index}`);
                return [];
            }

            emails.push(item.email);
        }

        return emails;
    } catch (error) {
        console.error(error.message);
        return [];
    }
}

module.exports = problem1;
